package com.amdocs;

public class Increment {
    private int counter = 1;
    private int temp = 0; // Declare "temp" as an instance variable

    public Increment() {
        if (counter == 0) {
            counter = 1;
        }
        temp = this.counter; // Initialize "temp" in the constructor
    }

    public int getCounter() {
        return counter++;
    }

    public int decreaseCounter(final int input) {
    if (input == 0) {
        counter--;
        temp = this.counter; 
    } else {
        if (input != 1) {
            counter++;
        }
        temp = this.counter; 
    }
    return temp;
}
}
