package com.amdocs;

import org.junit.Test;
import static org.junit.Assert.*;


public class CalculatorTest {

    @Test
    public void testAdd() {
        Calculator calculator = new Calculator();
        int result = calculator.add();
        assertEquals(9, result); // Expected result: 3 + 6 = 9
    }

    @Test
    public void testSub() {
        Calculator calculator = new Calculator();
        int result = calculator.sub();
        assertEquals(3, result); // Expected result: 6 - 3 = 3
    }
}
