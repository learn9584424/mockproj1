package com.amdocs;

import org.junit.Test;
import static org.junit.Assert.*;

public class IncrementTest{

    @Test
    public void testGetCounter() {
        Increment increment = new Increment();
        int result = increment.getCounter();
        assertEquals(1, result); // Initial value of counter is 1
    }

    @Test
    public void testDecreaseCounterWithInputZero() {
        Increment increment = new Increment();
        int result = increment.decreaseCounter(0);
        assertEquals(0, result); // Decreasing counter from 1 to 0
    }

    @Test
    public void testDecreaseCounterWithInputOne() {
        Increment increment = new Increment();
        int result = increment.decreaseCounter(1);
        assertEquals(1, result); // No change in counter (input is 1)
    }

    @Test
    public void testDecreaseCounterWithInputOtherThanZeroAndOne() {
        Increment increment = new Increment();
        int result = increment.decreaseCounter(2);
        assertEquals(2, result); // Increasing counter from 1 to 2
    }
}
